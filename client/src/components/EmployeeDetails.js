import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";
import * as styles from "../components/EmployeeDetails.module.css";

function EmployeeDetails(props) {

  return (
    <Modal
      show={true}
      onHide={() => props.setEmpDetailsShow()}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Header closeButton className={styles.header}>
        <Modal.Title id="contained-modal-title-vcenter">
          Employee Details :
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className={styles.body}>
        <h5>
          {props.employeeInfo.firstName.toUpperCase()}
          {" "}
          {props.employeeInfo.lastName.toUpperCase()}
        </h5><hr/>
        <table>
          <tr>
            <th>EMPLOYEE ID :</th>
            <td>{props.employeeInfo.empId}</td>
          </tr>
          <tr>
            <th>DATE OF JOINING : </th>
            <td>{props.employeeInfo.DOJ}</td>
          </tr>
          <tr>
            <th>DATE OF BIRTH :</th>
            <td>{props.employeeInfo.DOB}</td>
          </tr>
          <tr>
            <th>GENDER :</th>
            <td>{props.employeeInfo.gender}</td>
          </tr>
        </table>
        <h5>Address:</h5>
        <table>
          <tr>
            <th>DOOR NO :</th>
            <td> {props.employeeInfo.doorNo}</td>
          </tr>
          <tr>
            <th>STREET :</th>
            <td> {props.employeeInfo.street}</td>
          </tr>
          <tr>
            <th>LANDMARK :</th>
            <td> {props.employeeInfo.landmark}</td>
          </tr>
          <tr>
            <th>CITY :</th>
            <td> {props.employeeInfo.city}</td>
          </tr>
          <tr>
            <th>PINCODE :</th>
            <td> {props.employeeInfo.pincode}</td>
          </tr>
        </table>
      </Modal.Body>
      <div className={styles.footer}>
        <button onClick={() => props.setEmpDetailsShow()}>Close</button>
      </div>
    </Modal>
  );
}

export default EmployeeDetails;
