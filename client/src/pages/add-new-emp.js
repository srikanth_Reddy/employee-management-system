import React from "react";
import * as style from "../components/addNewEmp.module.css";
import axios from "axios";
import { useState } from "react";

import { Link } from "gatsby";

function AddNewEmp() {
  let [empId, setEmpId] = useState("");
  let [DOJ, setDOJ] = useState("");
  let [firstName, setFirstName] = useState("");
  let [lastName, setLastName] = useState("");
  let [DOB, setDOB] = useState("");
  let [gender,setGender] = useState("");
  let [doorNo, setDoorNo] = useState("");
  let [street, setStreet] = useState("");
  let [landmark, setLandmark] = useState("");
  let [city, setCity] = useState("");
  let [pincode, setPincode] = useState("");

  let [msg, setMsg] = useState(false);
  
  console.log(empId)

  let onSubmit = (event) => {
    let newEmpDetails = {
      empId: empId,
      DOJ: DOJ,
      firstName: firstName,
      lastName: lastName,
      DOB: DOB,
      gender: gender,
      doorNo: doorNo,
      street: street,
      landmark: landmark,
      city: city,
      pincode:pincode,
    };
    event.preventDefault();
    axios
      .post("http://localhost:5000/add-employee", {
        details: { ...newEmpDetails },
      })
      .then((res) => setMsg({ msg: res.data.msg }))
      .catch((res) => setMsg({ msg: "Adding Employee Failed" }));
  };

  return (
    <div className={style.body}>
      <div className={style.header}>
        <h2>New Employee Details</h2>
        <p>Please fill below details</p>
      </div>

      <form onSubmit={(event) => onSubmit(event)}>
        <div className={style.employmentDetails}>
          <div>
            <div>
              <span className={style.star}>*</span> Employee Id :
              <span className={style.required}>(required)</span>
            </div>
            <input type="text" name="empId"
            onChange={(e)=>setEmpId(e.target.value)}
             required />
          </div>

          <div>
            <div>
              <span className={style.star}>*</span>Date Of Joining :
              <span className={style.required}>(required)</span>
            </div>
            <input className={style.date} type="date" name="DOJ" 
            onChange={(e)=>setDOJ(e.target.value)}
            required />
          </div>
        </div>

        <div>
          <h3>Personal Information</h3>
          <hr />
          <div className={style.personalInfo}>
            <div>
              <div>
                <span className={style.star}>*</span>First Name:
                <span className={style.required}>(required)</span>
              </div>
              <input type="text" name="firstName" 
              onChange={(e)=>setFirstName(e.target.value)}
              required />
            </div>
            <div>
              <div>
                <span className={style.star}>*</span> Last Name:
                <span className={style.required}>(required)</span>
              </div>
              <input type="text" name="lastName" 
              onChange={(e)=>setLastName(e.target.value)}
              required />
            </div>

            <div>
              <div>
                <span className={style.star}>*</span>Date Of Birth:
                <span className={style.required}>(required)</span>
              </div>
              <input className={style.date} type="date" name="DOB" 
              onChange={(e)=>setDOB(e.target.value)}
              required />
            </div>

            <div>
              <div>
                Gender:<span className={style.required}>(optional)</span>
              </div>
              <div className={style.gender}
              onChange={(e)=>setGender(e.target.value)}
              >
                <div className={style.genderOpt}>
                  <div className={style.radio}>
                    <input type="radio" name="gender" value="male" />
                  </div>
                  <div> -Male</div>
                </div>
                <div className={style.genderOpt}>
                  <div className={style.radio}>
                    <input type="radio" name="gender" value="female" />
                  </div>
                  <div> -Female</div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <h3>Address</h3>
          <hr />

          <div className={style.address}>
            <div>
              <div>
                Flat.No/D.NO :<span className={style.required}>(optional)</span>
              </div>
              <input type="text" name="doorNo"
              onChange={(e)=>setDoorNo(e.target.value)}
               />
            </div>

            <div>
              <div>
                Street : <span className={style.required}>(optional)</span>
              </div>
              <input type="text" name="street"
              onChange={(e)=>setStreet(e.target.value)}
               />
            </div>

            <div>
              <div>
                Land Mark : <span className={style.required}>(optional)</span>
              </div>
              <input type="text" name="landmark" 
              onChange={(e)=>setLandmark(e.target.value)}
              />
            </div>

            <div>
              <div>
                City : <span className={style.required}>(optional)</span>
              </div>
              <input type="text" name="city" 
              onChange={(e)=>setCity(e.target.value)}
              />
            </div>

            <div>
              <div>
                Pincode : <span className={style.required}>(optional)</span>
              </div>
              <input type="text" name="pincode" 
              onChange={(e)=>setPincode(e.target.value)}
              />
            </div>
          </div>
        </div>

        <div className={style.btn}>
          <button type="submit">Submit</button>
        </div>
        {msg && (
          <>
            {msg.msg === "Adding Employee Failed" ? (
              <div style={{ color: "red" }}>{msg.msg}</div>
            ) : (
              <div className={style.successMsg}>{msg.msg}</div>
            )}
          </>
        )}
      </form>
      <button className={style.backButton}>
        <Link to="/">GO BACK</Link>
      </button>
    </div>
  );
}

export default AddNewEmp;
