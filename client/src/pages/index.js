import * as React from "react";
import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "gatsby";
import * as styles from "../components/index.module.css";
import EmployeeDetails from "../components/EmployeeDetails";

function IndexPage() {
  let [empDetailsShow, setEmpDetailsShow] = useState(false);
  let [employeeDetails, setEmployeeDetails] = useState(null);
  let [employees, setEmployees] = useState({
    loading: false,
    list: [],
    err: null,
  });

  useEffect(() => {
    setEmployees({ ...employees, loading: true });
    axios
      .get("http://localhost:5000/get-employees")
      .then((res) => {
        if (res.data.err) {
          setEmployees({
            ...employees,
            err: "Getting Employees Failed",
            loading: false,
          });
        } else {
          setEmployees({ loading: false, list: res.data });
        }
      })
      .catch((err) =>
        setEmployees({
          ...employees,
          err: "Getting Employees Failed",
          loading: false,
        })
      );
  }, []);

  let displayEmployeeDetails = (empId) => {
    setEmpDetailsShow(true);
    setEmployeeDetails(
      employees.list.filter((item) => empId === item.empId)[0]
    );
  };

  return (
    <div className={styles.body}>
      <div className={styles.header}>
        <h2>Employees</h2>
        <Link to="/add-new-emp">
          <button className={styles.btn}>+Add New Employee</button>
        </Link>
      </div>
      {employees.loading && <div>Loading...</div>}
      {employees.err && <div className={styles.err}>{employees.err}</div>}
      {!employees.err && (
        <div className={styles.employees}>
          <div className={styles.title}>
            <div>EmployeeNo</div>
            <div>Name</div>
            <div>Date-Of-Birth</div>
          </div>
          {employees.list.map((employee) => {
            return (
              <div className={styles.employee} key={employee.empId}>
                <div
                  className={styles.link}
                  onClick={() => displayEmployeeDetails(employee.empId)}
                >
                  {employee.empId}
                </div>
                <div>
                  {employee.firstName} {employee.lastName}
                </div>
                <div>{employee.DOB}</div>
              </div>
            );
          })}
        </div>
      )}
      {empDetailsShow && (
        <EmployeeDetails
          employeeInfo={employeeDetails}
          setEmpDetailsShow={() => setEmpDetailsShow(false)}
        />
      )}
    </div>
  );
}

export default IndexPage;
