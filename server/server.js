const express = require("express");
var fs = require("fs");
const fsPromises = require("fs").promises;
const app = express();
const cors = require("cors");
app.use(cors());
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/get-employees", (req, res) => {
  fsPromises
    .readFile("./data.json")
    .then((data) => JSON.parse(data))
    .then((result) => res.json(result))
    .catch((err) => {
      console.log(err);
      res.json({ err: err });
    });
});

app.post("/add-employee", (req, res) => {
  fsPromises
    .readFile("./data.json")
    .then((data) => JSON.parse(data))
    .then((result) => {
      let newData = [...result, req.body.details];
      fsPromises.writeFile("./data.json", JSON.stringify(newData));
      res.json({ msg: "Employee added successfully" });
    })
    .catch((err) => {
      console.log(err);
      res.json({ msg: "Adding Employee Failed" });
    });
});

app.listen(5000, () => {
  console.log("server started on port 5000");
});
